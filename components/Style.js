import {StyleSheet} from 'react-native'

export default {
    color: '#A2273C',
    container: {
        margin: 20
    },
    title: {
        fontSize: 22,
        marginBottom: 20
    },
    button: {
        backgroundColor: '#A2273C',
        color: '#FFFFFF'
    },
    input: {height: 40, borderColor: 'gray', borderWidth: 1, paddingHorizontal: 10, marginBottom: 20},

    header: {
        backgroundColor: '#A2273C'
    },
    headerTitle: {
        color: '#FFFFFF'
    },
    item : {
      backgroundColor: '#A2273C',
      width : 260,
      height: 100,
      marginBottom: 5,


    }

}


