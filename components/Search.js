import React from 'react'
import { TextInput, Button, View } from 'react-native'
import style from './Style'
import { createStackNavigator } from '@react-navigation/stack'
import List from './List'

const Stack = createStackNavigator();

class Search extends React.Component {

    static navigationOptions = {
        title: 'Rechercher une ville',
        headerStyle: style.header,
        headerTitleStyle: style.headerTitle
    }

    constructor(props) {
        super(props)
        this.state = {
            city: 'Nancy'
        }
    }


    setCity(city) {
        this.setState({ city })
    }

    submit() {
        this.props.navigation.navigate('Result', { city: this.state.city })
    }

    render() {
        return (
            <View style={style.container}>

                <TextInput
                    onchangeText={(text) => this.setCity(text)}
                    style={style.input}
                    defaultValue={this.state.city}
                />
                <Button color={style.color} onPress={() => this.submit()} title="Rechercher" />
            </View>
        )

    }


}
/*
const navigationOptions = {
    navigationOption,
    headerStyle: style.header,
    headerTitleStyle: style.headerTitle
}
*/

export default function MyStack() {
    return (
        <Stack.Navigator>
            
            <Stack.Screen name="Result" component={List} options={List.navigationOptions} />
            <Stack.Screen name="Search" component={Search} options={Search.navigationOptions} />
        </Stack.Navigator>
    )
}