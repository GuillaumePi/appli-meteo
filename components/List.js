import React from 'react'
import { Text, ActivityIndicator, FlatList, View } from 'react-native'
import style from './Style'
import axios from 'axios'
import moment from 'moment'

export default class List extends React.Component {

    static navigationOptions = (params) => {
        console.log(params)
        return {

            //title: `Météo / ${params.route.params.city}`,
            headerStyle: style.header,
            headerTitleStyle: style.headerTitle
        }
    }

    constructor(props) {
        super(props)
        //console.log('state', this.props.route.params.city)
        this.state = {
            city: 'Nancy', //this.props.route.params.city,
            report: null,
            data: []
        }
        this.fetchWeather();
    }

    //   async componentDidMount () {
    //     const response = await fetch ('https://maps.open-street/api/geocoding/?address=%2C+Nancy%2C+France&sensor=false&key=d394b44deca835a946b86ef89eed7271')
    //   const lieux = await response.json ()
    // this.setState ({data: lieux})
    //}

    


    fetchWeather() {
        axios.get('https://api.meteo-concept.com/api/forecast/daily?token=6805afb9e484141a2ae84fe8176e3c6ba8a271748fd66c929eb812f27455f92a&latlng=48.692054,6.184417')
            .then((response) => {
                this.setState({ report: response.data })
            });
    }

    render() {
        tableWeather = {
            0: "Soleil",
            1: "Peu nuageux",
            2: "Ciel voilé",
            3: "Nuageux",
            4: "Très nuageux",
            5: "Couvert",
            6: "Brouillard",
            7: "Brouillard givrant",
            10: "Pluie faible",
            11: "Pluie modérée",
            12: "Pluie forte",
            13: "Pluie faible verglaçante",
            14: "Pluie modérée verglaçante",
            15: "Pluie forte verglaçante",
            16: "Bruine",
            20: "Neige faible",
            21: "Neige modérée",
            22: "Neige forte",
            30: "Pluie et neige mêlées faibles",
            31: "Pluie et neige mêlées modérées",
            32: "Pluie et neige mêlées fortes",
            40: "Averses de pluie locales et faibles",
            41: "Averses de pluie locales",
            42: "Averses locales et fortes",
            43: "Averses de pluie faibles",
            44: "Averses de pluie",
            45: "Averses de pluie fortes",
            46: "Averses de pluie faibles et fréquentes",
            47: "Averses de pluie fréquentes",
            48: "Averses de pluie fortes et fréquentes",
            60: "Averses de neige localisées et faibles",
            61: "Averses de neige localisées",
            62: "Averses de neige localisées et fortes",
            63: "Averses de neige faibles",
            64: "Averses de neige",
            65: "Averses de neige fortes",
            66: "Averses de neige faibles et fréquentes",
            67: "Averses de neige fréquentes",
            68: "Averses de neige fortes et fréquentes",
            70: "Averses de pluie et neige mêlées localisées et faibles",
            71: "Averses de pluie et neige mêlées localisées",
            72: "Averses de pluie et neige mêlées localisées et fortes",
            73: "Averses de pluie et neige mêlées faibles",
            74: "Averses de pluie et neige mêlées",
            75: "Averses de pluie et neige mêlées fortes",
            76: "Averses de pluie et neige mêlées faibles et nombreuses",
            77: "Averses de pluie et neige mêlées fréquentes",
            78: "Averses de pluie et neige mêlées fortes et fréquentes",
            100: "Orages faibles et locaux",
            101: "Orages locaux",
            102: "Orages fort et locaux",
            103: "Orages faibles",
            104: "Orages",
            105: "Orages forts",
            106: "Orages faibles et fréquents",
            107: "Orages fréquents",
            108: "Orages forts et fréquents",
            120: "Orages faibles et locaux de neige ou grésil",
            121: "Orages locaux de neige ou grésil",
            122: "Orages locaux de neige ou grésil",
            123: "Orages faibles de neige ou grésil",
            124: "Orages de neige ou grésil",
            125: "Orages de neige ou grésil",
            126: "Orages faibles et fréquents de neige ou grésil",
            127: "Orages fréquents de neige ou grésil",
            128: "Orages fréquents de neige ou grésil",
            130: "Orages faibles et locaux de pluie et neige mêlées ou grésil",
            131: "Orages locaux de pluie et neige mêlées ou grésil",
            132: "Orages fort et locaux de pluie et neige mêlées ou grésil",
            133: "Orages faibles de pluie et neige mêlées ou grésil",
            134: "Orages de pluie et neige mêlées ou grésil",
            135: "Orages forts de pluie et neige mêlées ou grésil",
            136: "Orages faibles et fréquents de pluie et neige mêlées ou grésil",
            137: "Orages fréquents de pluie et neige mêlées ou grésil",
            138: "Orages forts et fréquents de pluie et neige mêlées ou grésil",
            140: "Pluies orageuses",
            141: "Pluie et neige mêlées à caractère orageux",
            142: "Neige à caractère orageux",
            210: "Pluie faible intermittente",
            211: "Pluie modérée intermittente",
            212: "Pluie forte intermittente",
            220: "Neige faible intermittente",
            221: "Neige modérée intermittente",
            222: "Neige forte intermittente",
            230: "Pluie et neige mêlées",
            231: "Pluie et neige mêlées",
            232: "Pluie et neige mêlées",
            235: "Averses de grêle",
        }


        tableWeatherLogo = {
            0:"../assets/iconWeather/day.svg",
            1:"../assets/iconWeather/cloudy-day-1.svg",
            2:"../assets/iconWeather/cloudy-day-2.svg",
            3:"../assets/iconWeather/cloudy-day-2.svg",
            4:"../assets/iconWeather/cloudy-day-3.svg",
            5:"../assets/iconWeather/cloudy.svg",
            6:"../assets/iconWeather/cloudy.svg",
            7:"../assets/iconWeather/cloudy.svg",
            10:"../assets/iconWeather/rainy-4.svg",
            11:"../assets/iconWeather/rainy-5.svg",
            12:"../assets/iconWeather/rainy-6.svg",
            13:"../assets/iconWeather/rainy-4.svg",
            14:"../assets/iconWeather/rainy-5.svg",
            15:"../assets/iconWeather/rainy-6.svg",
            16:"../assets/iconWeather/rainy-5.svg",
            20:"../assets/iconWeather/snowy-4.svg",
            21:"../assets/iconWeather/snowy-5.svg",
            22:"../assets/iconWeather/snowy-6.svg",
            30:"../assets/iconWeather/snowy-4.svg",
            31:"../assets/iconWeather/snowy-5.svg",
            32:"../assets/iconWeather/snowy-6.svg",
            40:"../assets/iconWeather/rainy-1.svg",
            41:"../assets/iconWeather/rainy-1.svg",
            42:"../assets/iconWeather/rainy-1.svg",
            43:"../assets/iconWeather/rainy-2.svg",
            44:"../assets/iconWeather/rainy-2.svg",
            45:"../assets/iconWeather/rainy-3.svg",
            46:"../assets/iconWeather/rainy-2.svg",
            47:"../assets/iconWeather/rainy-2.svg",
            48:"../assets/iconWeather/rainy-3.svg",
            60:"../assets/iconWeather/snowy-1.svg",
            61:"../assets/iconWeather/snowy-1.svg",
            62:"../assets/iconWeather/snowy-1.svg",
            63:"../assets/iconWeather/snowy-1.svg",
            64:"../assets/iconWeather/snowy-1.svg",
            65:"../assets/iconWeather/snowy-1.svg",
            66:"../assets/iconWeather/snowy-1.svg",
            67:"../assets/iconWeather/snowy-1.svg",
            68:"../assets/iconWeather/snowy-1.svg",
            70:"../assets/iconWeather/snowy-1.svg",
            71:"../assets/iconWeather/snowy-1.svg",
            72:"../assets/iconWeather/snowy-1.svg",
            73:"../assets/iconWeather/snowy-1.svg",
            74:"../assets/iconWeather/snowy-1.svg",
            75:"../assets/iconWeather/snowy-1.svg",
            76:"../assets/iconWeather/snowy-1.svg",
            77:"../assets/iconWeather/snowy-1.svg",
            78:"../assets/iconWeather/snowy-1.svg",
            100:"../assets/iconWeather/thunder.svg",
            101:"../assets/iconWeather/thunder.svg",
            102:"../assets/iconWeather/thunder.svg",
            103:"../assets/iconWeather/thunder.svg",
            104:"../assets/iconWeather/thunder.svg",
            105:"../assets/iconWeather/thunder.svg",
            106:"../assets/iconWeather/thunder.svg",
            107:"../assets/iconWeather/thunder.svg",
            108:"../assets/iconWeather/thunder.svg",
            120:"../assets/iconWeather/thunder.svg",
            121:"../assets/iconWeather/thunder.svg",
            122:"../assets/iconWeather/thunder.svg",
            123:"../assets/iconWeather/thunder.svg",
            124:"../assets/iconWeather/thunder.svg",
            125:"../assets/iconWeather/thunder.svg",
            126:"../assets/iconWeather/thunder.svg",
            127:"../assets/iconWeather/thunder.svg",
            128:"../assets/iconWeather/thunder.svg",
            130:"../assets/iconWeather/thunder.svg",
            131:"../assets/iconWeather/thunder.svg",
            132:"../assets/iconWeather/thunder.svg",
            133:"../assets/iconWeather/thunder.svg",
            134:"../assets/iconWeather/thunder.svg",
            135:"../assets/iconWeather/thunder.svg",
            136:"../assets/iconWeather/thunder.svg",
            137:"../assets/iconWeather/thunder.svg",
            138:"../assets/iconWeather/thunder.svg",
            140:"../assets/iconWeather/rainy-1.svg",
            141:"../assets/iconWeather/rainy-1.svg",
            142:"../assets/iconWeather/rainy-1.svg",
            210:"../assets/iconWeather/rainy-1.svg",
            211:"../assets/iconWeather/rainy-1.svg",
            212:"../assets/iconWeather/rainy-1.svg",
            220:"../assets/iconWeather/rainy-1.svg",
            221:"../assets/iconWeather/rainy-1.svg",
            222:"../assets/iconWeather/rainy-1.svg",
            230:"../assets/iconWeather/rainy-1.svg",
            231:"../assets/iconWeather/rainy-1.svg",
            232:"../assets/iconWeather/rainy-1.svg",
            235:"../assets/iconWeather/rainy-7.svg",
        };

        if (this.state.report === null) {
            return (
                <ActivityIndicator color={style.color} size="large" />
            )
        } else {
            console.log(this.state.report)
            moment.locale('fr');
            return (
                <FlatList
                    data={this.state.report.forecast}
                    keyExtractor={(item) => item.day.toString()}
                    renderItem={({ item }) => (
                        <View style={style.item}>
                            <Text>{moment(item.datetime).format('dddd DD MMMM')}</Text>
                            <Text>{tableWeather[item.weather]}</Text>
                            <Image 
                                source={require(tableWeatherLogo[item.weather])}/>
                            <Text>{item.tmin}°C</Text>
                            <Text>{item.tmax}°C</Text>
                        </View>
                    )} />
            )
        }
    }

}

