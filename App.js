
import React from 'react';
import { View, StyleSheet } from 'react-native';
import About from './components/About';
import Search from './components/Search';
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { NavigationContainer} from '@react-navigation/native';
import {StatusBar} from 'react-native'

const Tabs = createBottomTabNavigator ();

export default class App extends React.Component {
  render() {
   return (
   <NavigationContainer>
   <StatusBar hidden={true}/>
    <Tabs.Navigator
    tabBarOptions={{
     labelStyle: {textAlign: 'center'}
    }}
    >
    <Tabs.Screen name='Search' component={Search}/>
    <Tabs.Screen name='About' component={About}/>
     </Tabs.Navigator>
     </NavigationContainer>

  );
}
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tab : {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
  }
  });
